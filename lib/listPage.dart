import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ListPage extends StatefulWidget {
  @override
  _ListPage createState() => _ListPage();
}

class _ListPage extends State<ListPage> {
  Future<List<UserDTO>> users;

  @override
  void initState() {
    super.initState();
    users = readUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("list"),
        ),
        body: FutureBuilder(
            future: users,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        title: Text(snapshot.data[index].name),
                        onTap: () {
                          Navigator.pushNamed(context, '/user',
                              arguments: snapshot.data[index]);
                        },
                      );
                    });
              } else {
                return Text("...");
              }
            }));
  }

  Future<List<UserDTO>> readUsers() async {
    final response =
        await http.get(Uri.https('jsonplaceholder.typicode.com', '/users'));
    if (response.statusCode == 200) {
      Iterable l = json.decode(response.body);
      List<UserDTO> list = l.map((e) => UserDTO.fromJSON(e)).toList();
      return list;
    } else {
      throw Exception("cannot read from internet");
    }
  }
}

class UserDTO {
  final int id;
  final String name;

  UserDTO({this.id, this.name});

  factory UserDTO.fromJSON(Map<String, dynamic> json) {
    return UserDTO(id: json['id'], name: json['name']);
  }
}
