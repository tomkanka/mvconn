import 'package:flutter/material.dart';

import 'listPage.dart';

class UserWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UserDTO param = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          title: Text("User"),
        ),
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  (param != null)
                      ? param.id.toString() + ": " + param.name
                      : "___EMPTY___",
                )
              ]),
        ));
  }
}
